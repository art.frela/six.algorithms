package quick

import (
	"slices"
	"testing"

	"gitlab.com/art.frela/six.algorithms/utils/genslice"
)

func TestSort(t *testing.T) {
	tests := []struct {
		name  string
		slice []int
	}{
		{
			"test.1 100 max:100",
			genslice.MakeRandomSlice(100, 1000),
		},
		{
			"test.2 0 max:1000",
			genslice.MakeRandomSlice(0, 10),
		},
		{
			"test.3 5000 max:10000",
			genslice.MakeRandomSlice(5_000, 10_000),
		},
		{
			"test.4 1500 max:1000",
			genslice.MakeRandomSlice(1_500, 1_000),
		},
		{
			"test.5 50000 max:1000",
			genslice.MakeRandomSlice(50_000, 1_000),
		},
		// {
		// 	"test.6 1_000_000 max:9",
		// 	genslice.MakeRandomSlice(1_000_000, 9),
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if slices.IsSorted(tt.slice) {
				return
			}

			Sort(tt.slice)

			if !slices.IsSorted(tt.slice) {
				t.Error("Sort(tt.slice) not sorted, expected sorted")
			}
		})
	}
}

const numItems = 15_000

var slice = genslice.MakeRandomSlice(numItems, 10_000)

func BenchmarkSort(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		Sort(target)
	}
}

/*
goos: darwin
goarch: amd64
pkg: gitlab.com/art.frela/six.algorithms/sortandsearch/quick
cpu: Intel(R) Core(TM) i5-3470S CPU @ 2.90GHz
BenchmarkSort-4   	            1365	    869398 ns/op	      90 B/op	       0 allocs/op
BenchmarkSortSlices-4   	    1161	    982852 ns/op	     105 B/op	       0 allocs/op
BenchmarkSortStd-4   	         632	   1896166 ns/op	     218 B/op	       1 allocs/op
BenchmarkQuicksort-4   	        1309	    868521 ns/op	      93 B/op	       0 allocs/op
*/

func BenchmarkSortSlices(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		SortSlices(target)
	}
}

func BenchmarkSortStd(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		SortStd(target)
	}
}

func BenchmarkQuicksort(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		Quicksort(target)
	}
}
