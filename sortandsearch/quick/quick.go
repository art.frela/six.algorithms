package quick

import (
	"sort"

	"golang.org/x/exp/slices"
)

// Sort sorts passed slice by quicksort algorithm,
// described in Rod Stephens "Essential Algorithms" 6th chapter.
func Sort(slice []int) {
	// implement me
	if len(slice) <= 1 {
		return
	}

	qsort(slice, 0, len(slice)-1)
}

func qsort(slice []int, start, end int) {
	if start >= end {
		return
	}

	devider := slice[start]

	lo, hi := start, end

loop:
	for {
		// from hi
		for slice[hi] >= devider {
			hi--
			if hi <= lo {
				break loop
			}
		}

		if hi <= lo { // means we end, put devider and return
			slice[lo] = devider
			break loop
		}

		// put found value to hole
		slice[lo], slice[hi] = slice[hi], slice[lo]

		//
		lo++
		// from lo
		for slice[lo] < devider {
			lo++
			if lo >= hi {
				slice[hi] = devider
				break loop
			}
		}
		//
		if lo >= hi {
			lo = hi
			slice[hi] = devider
			break loop
		}

		slice[hi], slice[lo] = slice[lo], slice[hi]
	}

	qsort(slice, start, lo-1)
	qsort(slice, lo+1, end)
}

// SortSlices sorts slice of ints
// by using golang.org/x/exp/slices pkg.
func SortSlices(slice []int) {
	slices.Sort(slice)
}

// SortStd sorts slice of ints
// by using std sort pkg.
func SortStd(slice []int) {
	sort.Ints(slice)
}

// Quicksort sorts passed slice by quicksort algorithm,
// described in https://en.wikipedia.org/wiki/Quicksort#Lomuto_partition_scheme.
func Quicksort(arr []int) {
	// See if we should stop the recursion.
	if len(arr) < 2 {
		return
	}

	// Partition.
	pivotIX := partition(arr)

	// Recursively sort the two halves.
	Quicksort(arr[0:pivotIX])
	Quicksort(arr[pivotIX+1:])
}

// Partition the slice.
func partition(arr []int) int {
	// Set the lower and upper indexes.
	lo := 0
	hi := len(arr) - 1

	// Use the last element as the pivot.
	pivot := arr[hi]

	// Temporary pivot index
	i := lo - 1

	for j := lo; j < hi; j++ {
		// See if arr[j] <= pivot.
		if arr[j] <= pivot {
			// Move the temporary pivot index forward
			i = i + 1

			// Swap arr[i] and arr[j].
			arr[i], arr[j] = arr[j], arr[i]
		}
	}

	// Drop the pivot in the between the two halves.
	i = i + 1
	arr[i], arr[hi] = arr[hi], arr[i]

	// Return the pivot's index.
	return i
}
