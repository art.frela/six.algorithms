package binary

import (
	"testing"
)

func TestSearch(t *testing.T) {
	// 23 items, max iterations = 5 (23/2 = 12, 12/2 = 6, 6/2 = 3, 3/2 = 2, 2/2 = 2)
	source := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 22, 32, 34, 42, 45, 55, 56, 57, 66, 77, 78, 88, 99}

	tests := []struct {
		name         string
		slice        []int
		target       int
		wantIndex    int
		wantNumTests int
	}{
		{
			"test.1 ok",
			source,
			11,
			9,
			4,
		},
		{
			"test.2 left border",
			source,
			0,
			0,
			4,
		},
		{
			"test.3 right border",
			source,
			99,
			22,
			5,
		},
		{
			"test.4 not found",
			source,
			200,
			-1,
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotIndex, gotNumTests := Search(tt.slice, tt.target)
			if gotIndex != tt.wantIndex {
				t.Errorf("Search() gotIndex = %v, want %v", gotIndex, tt.wantIndex)
			}
			if gotNumTests != tt.wantNumTests {
				t.Errorf("Search() gotNumTests = %v, want %v", gotNumTests, tt.wantNumTests)
			}
		})
	}
}
