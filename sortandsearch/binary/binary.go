package binary

// Search performs binary search.
// Return the target's location in the slice and the number of tests.
// If the item is not found, return -1 and the number tests.
func Search(slice []int, target int) (index, numTests int) {
	min := 0
	max := len(slice) - 1

	iter := 0
	for min <= max {
		iter++
		mid := (min + max) / 2
		if target < slice[mid] {
			max = mid - 1
		} else if target > slice[mid] {
			min = mid + 1
		} else {
			return mid, iter
		}
	}

	return -1, iter
}
