package count

import "testing"

func TestSort(t *testing.T) {
	tests := []struct {
		name  string
		slice []Customer
	}{
		{
			"test.1 100 max:100",
			MakeRandomSlice(100, 100),
		},
		{
			"test.2 0 max:10",
			MakeRandomSlice(0, 10),
		},
		{
			"test.3 5000 max:50",
			MakeRandomSlice(5_000, 50),
		},
		{
			"test.4 1500 max:1000",
			MakeRandomSlice(1_500, 1_000),
		},
		{
			"test.5 50000 max:100",
			MakeRandomSlice(50_000, 100),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if checkSorted(tt.slice) {
				return
			}

			Sort(tt.slice)

			if !checkSorted(tt.slice) {
				t.Error("Sort(tt.slice) not sorted, expected sorted")
			}

			PrintSlice(tt.slice, 0)
		})
	}
}

func TestCountingSort(t *testing.T) {
	tests := []struct {
		name     string
		numItems int
		max      int
	}{
		{
			"test.1 100 max:100",
			100,
			100,
		},
		{
			"test.2 0 max:10",
			0, 10,
		},
		{
			"test.3 5000 max:50",
			5_000, 50,
		},
		{
			"test.4 1500 max:1000",
			1_500, 1_000,
		},
		{
			"test.5 50000 max:100",
			50_000, 100,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			slice := MakeRandomSlice(tt.numItems, tt.max)

			if checkSorted(slice) {
				return
			}

			CountingSort(slice, tt.max)

			if !checkSorted(slice) {
				t.Error("CountingSort(tt.slice) not sorted, expected sorted")
			}

			PrintSlice(slice, 0)
		})
	}
}
