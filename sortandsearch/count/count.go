package count

type Customer struct {
	id           string
	numPurchases int
}

func Sort(slice []Customer) {
	counts := make(map[int][]Customer)

	maxPurchases := 0
	for i := 0; i < len(slice); i++ {
		counts[slice[i].numPurchases] = append(counts[slice[i].numPurchases], slice[i])
		if maxPurchases < slice[i].numPurchases {
			maxPurchases = slice[i].numPurchases
		}
	}

	ix := 0
	for i := 0; i <= maxPurchases; i++ {
		customers := counts[i]
		for j := 0; j < len(customers); j++ {
			slice[ix] = customers[j]
			ix++
		}
	}
}

// CountingSort sorts an array containing items in [0, max).
func CountingSort(arr []Customer, max int) {
	numItems := len(arr)

	// Make an array to hold counts.
	counts := make([]int, max)

	// Count the values.
	for i := 0; i < numItems; i++ {
		counts[arr[i].numPurchases]++
	}

	// Convert counts into counts of values <=.
	for i := 1; i < max; i++ {
		counts[i] += counts[i-1]
	}

	// Count out the values.
	result := make([]Customer, numItems)
	for i := numItems - 1; i >= 0; i-- {
		// Move item i into position.
		num := arr[i].numPurchases
		result[counts[num]-1] = arr[i]
		counts[num] -= 1
	}

	copy(arr, result)
}
