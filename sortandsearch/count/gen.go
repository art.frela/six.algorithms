package count

import (
	"fmt"
	"math/rand"
	"strconv"
)

const defNumItems = 10

// MakeRandomSlice creates []Customer with length numItems, filled pseudo rand int.
func MakeRandomSlice(numItems, max int) []Customer {
	out := make([]Customer, numItems)

	for i := 0; i < numItems; i++ {
		num := rand.Intn(max)
		out[i] = Customer{
			id:           "C" + strconv.Itoa(num),
			numPurchases: num,
		}
	}

	return out
}

// PrintSlice prints to stdout first numItems elements from slice,
// if numItems <=0 then use 10.
func PrintSlice(slice []Customer, numItems int) {
	if numItems <= 0 {
		numItems = defNumItems
	}

	if len(slice) <= numItems {
		numItems = len(slice)
	}

	out := make([]string, numItems)

	for i := 0; i < numItems; i++ {
		out[i] = slice[i].id
	}

	fmt.Printf("%+v\n", out)
}

// Verify that the slice is sorted.
func CheckSorted(slice []Customer) {
	if !checkSorted(slice) {
		fmt.Println("The slice is NOT sorted!")
		return
	}

	fmt.Println("The slice is sorted")
}

// Verify that the slice is sorted.
func checkSorted(slice []Customer) bool {
	for i := len(slice) - 1; i > 0; i-- {
		if slice[i].numPurchases < slice[i-1].numPurchases {
			return false
		}
	}

	return true
}
