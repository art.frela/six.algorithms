package bubble

import (
	"slices"
	"testing"

	"gitlab.com/art.frela/six.algorithms/utils/genslice"
)

func TestSort(t *testing.T) {
	tests := []struct {
		name  string
		slice []int
	}{
		{
			"test.1 100 max:1000",
			genslice.MakeRandomSlice(100, 1000),
		},
		{
			"test.2 0 max:1000",
			genslice.MakeRandomSlice(0, 1000),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if slices.IsSorted(tt.slice) {
				return
			}

			Sort(tt.slice)

			if !slices.IsSorted(tt.slice) {
				t.Error("Sort(tt.slice) not sorted, expected sorted")
			}
		})
	}
}

func TestSortOpt(t *testing.T) {
	tests := []struct {
		name  string
		slice []int
	}{
		{
			"test.1 100 max:1000",
			genslice.MakeRandomSlice(100, 1000),
		},
		{
			"test.2 0 max:1000",
			genslice.MakeRandomSlice(0, 10),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if slices.IsSorted(tt.slice) {
				return
			}

			SortOpt(tt.slice)

			if !slices.IsSorted(tt.slice) {
				t.Error("Sort(tt.slice) not sorted, expected sorted")
			}
		})
	}
}

func TestSortOpt2(t *testing.T) {
	tests := []struct {
		name  string
		slice []int
	}{
		{
			"test.1 100 max:100",
			genslice.MakeRandomSlice(100, 100),
		},
		{
			"test.2 0 max:1000",
			genslice.MakeRandomSlice(0, 10),
		},
		{
			"test.3 5000 max:10000",
			genslice.MakeRandomSlice(5_000, 10_000),
		},
		{
			"test.4 1500 max:1000",
			genslice.MakeRandomSlice(1_500, 1_000),
		},
		{
			"test.5 50000 max:1000",
			genslice.MakeRandomSlice(50_000, 1_000),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if slices.IsSorted(tt.slice) {
				return
			}

			ShakerSort(tt.slice)

			if !slices.IsSorted(tt.slice) {
				t.Error("Sort(tt.slice) not sorted, expected sorted")
			}
			genslice.PrintSlice(tt.slice, 0)
		})
	}
}

const numItems = 15_000

var slice = genslice.MakeRandomSlice(numItems, 10_000)

func BenchmarkSort(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		Sort(target)
	}
}

/*
goos: darwin
goarch: amd64
pkg: gitlab.com/art.frela/six.algorithms/sortandsearch/bubble
cpu: Intel(R) Core(TM) i5-3470S CPU @ 2.90GHz
15_000 items

BenchmarkSort-4   	               3	 348707544 ns/op	   81920 B/op	       0 allocs/op
BenchmarkSortOpt-4   	           4	 317411423 ns/op	   61440 B/op	       0 allocs/op
BenchmarkShakerSort-4   	       5	 204718287 ns/op	   24577 B/op	       0 allocs/op
BenchmarkShakerSort-4   	       5	 205614343 ns/op	   24576 B/op	       0 allocs/op
BenchmarkShakerSort-4   	       5	 210654513 ns/op	   24576 B/op	       0 allocs/op
*/

func BenchmarkSortOpt(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		SortOpt(target)
	}
}

func BenchmarkShakerSort(b *testing.B) {
	target := make([]int, len(slice))

	for i := 0; i < b.N; i++ {
		copy(target, slice)
		ShakerSort(target)
	}
}
