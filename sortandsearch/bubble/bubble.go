/*
	Package bubble

There are several sorting algorithms that have better than N^2 performance.
For example, quicksort (which you’ll study in the next milestone), merge sort,
and heap sort all have N * log2(N) performance, which is much faster than N2.
So why bother with bubble sort?

First, it’s interesting.
The study of algorithms is in no small part an exercise in learning new techniques,
exercising your brain muscles, and getting complex algorithms to work.
Bubble sort is relatively simple, so it makes a good place to start your study of sorting algorithms.

Second, there are actually two situations when bubble sort usually beats “faster” algorithms such as quicksort.

If the slice is very small, then the higher overhead needed by more complicated algorithms slows them down.
So bubble sort is often faster. If the slice contains fewer than around 5 items, bubble sort often wins.
Of course, if the slice holds only 5 items, it doesn’t matter too much which algorithm you use unless
you’re going to sort tiny slices many, many, MANY times.

The second case in which bubble sort is fastest is when the slice is already mostly sorted.
For example, suppose you have 1 million customer records and 6 of them are in the wrong places.

Implementaions:

  - Sort(slice []int) - not optimized N^2
  - SortOpt(slice []int) - optimized one way - sinking down
  - ShakerSort(slice []int) - fully optimized
*/
package bubble

// Sort sorts slice not optimized N^2.
func Sort(slice []int) {
	notSorted := true

	for notSorted {
		notSorted = false

		for i := 1; i < len(slice); i++ {
			if slice[i-1] > slice[i] {
				slice[i-1], slice[i] = slice[i], slice[i-1]
				notSorted = true
			}
		}
	}
}

// SortOpt sorts slice optimized one way - sinking down, N^2.
func SortOpt(slice []int) {
	notSorted := true
	lenSlice := len(slice)

	for notSorted {
		notSorted = false

		for i := 1; i < lenSlice; i++ {
			if slice[i-1] > slice[i] {
				slice[i-1], slice[i] = slice[i], slice[i-1]
				notSorted = true
			}
		}
		lenSlice--
	}
}

// ShakerSort sorts slice fully optimized, worst N^2,
// become closer O(n) if the slice is mostly ordered.
func ShakerSort(slice []int) {
	rightBorder := len(slice)
	newRightBorder := rightBorder
	leftBorder := 1

	for newRightBorder > 0 && leftBorder > 0 {
		newRightBorder = 0
		// sinking down
		for i := leftBorder; i < rightBorder; i++ {
			if slice[i-1] > slice[i] {
				slice[i-1], slice[i] = slice[i], slice[i-1]
				newRightBorder = i
			}
		}

		rightBorder = newRightBorder

		if newRightBorder == 0 {
			return
		}

		newLeftBorder := 0
		// bubbling up
		for j := rightBorder - 1; j > leftBorder-1; j-- {
			if slice[j] < slice[j-1] {
				slice[j-1], slice[j] = slice[j], slice[j-1]
				newLeftBorder = j
			}
		}

		leftBorder = newLeftBorder
	}
}
