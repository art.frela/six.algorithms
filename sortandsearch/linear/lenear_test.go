package linear

import (
	"testing"
)

func TestSearch(t *testing.T) {
	source := []int{99, 88, 55, 66, 77, 22, 11, 34, 45, 56, 57, 78, 1, 2, 3, 4, 5, 6, 7, 8, 0, 77, 32, 42}

	tests := []struct {
		name         string
		slice        []int
		target       int
		wantIndex    int
		wantNumTests int
	}{
		{
			"test.1 ok",
			source,
			11,
			6,
			7,
		},
		{
			"test.2 left border",
			source,
			99,
			0,
			1,
		},
		{
			"test.3 right border",
			source,
			42,
			23,
			24,
		},
		{
			"test.4 not found",
			source,
			200,
			-1,
			24,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotIndex, gotNumTests := Search(tt.slice, tt.target)
			if gotIndex != tt.wantIndex {
				t.Errorf("Search() gotIndex = %v, want %v", gotIndex, tt.wantIndex)
			}
			if gotNumTests != tt.wantNumTests {
				t.Errorf("Search() gotNumTests = %v, want %v", gotNumTests, tt.wantNumTests)
			}
		})
	}
}
