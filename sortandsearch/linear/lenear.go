package linear

func Search(slice []int, target int) (index, numTests int) {
	for i := 0; i < len(slice); i++ {
		if slice[i] == target {
			return i, i + 1
		}
	}

	return -1, len(slice)
}
