// Package factorial contains func implements n! calculation.
package factorial

// Factorial calculates n! factorial,
// max n = 20 for int64.
func Factorial(n int64) int64 {
	if n == 0 {
		return 1
	}

	return n * Factorial(n-1)
}
