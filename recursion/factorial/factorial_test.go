// Package factorial contains func implements n! calculation.
package factorial

import "testing"

func TestFactorial(t *testing.T) {
	tests := []struct {
		name string
		n    int64
		want int64
	}{
		{
			"test.1 zero",
			0,
			1,
		},
		{
			"test.2 6",
			6,
			720,
		},
		{
			"test.3 21 more then maxN",
			21,
			-4249290049419214848,
		},
		{
			"test.4 20 maxN",
			20,
			2_432_902_008_176_640_000,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Factorial(tt.n); got != tt.want {
				t.Errorf("Factorial() = %v, want %v", got, tt.want)
			}
		})
	}
}
