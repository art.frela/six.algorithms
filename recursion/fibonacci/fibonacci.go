package fibonacci

// Fib calculates Fibonacci numbers grow, recursion style.
func Fib(n int64) int64 {
	if n == 0 {
		return 0
	}

	if n == 1 {
		return 1
	}

	return Fib(n-1) + Fib(n-2)
}

// FibAlt calculates Fibonacci numbers grow without recursion.
func FibAlt(n int64) int64 {
	if n == 0 {
		return 0
	}

	if n == 1 {
		return 1
	}

	var prepre int64
	var pre int64 = 1
	res := prepre + pre

	var i int64 = 2
	for ; i < n; i++ {
		prepre, pre = pre, res
		res = prepre + pre
	}

	return res
}
