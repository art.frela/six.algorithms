package fibonacci

import "testing"

func TestFib(t *testing.T) {
	tests := []struct {
		name string
		n    int64
		want int64
	}{
		{
			"test.1 Fib(10)",
			10,
			55,
		},
		{
			"test.2 Fib(0)",
			0,
			0,
		},
		{
			"test.3 Fib(1)",
			1,
			1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Fib(tt.n)
			got1 := FibAlt(tt.n)
			if got != tt.want {
				t.Errorf("Fib() = %v, want %v", got, tt.want)
			}
			if got1 != tt.want {
				t.Errorf("FibAlt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkFib40(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Fib(40)
	}
}

/*
goos: darwin
goarch: amd64
pkg: gitlab.com/art.frela/six.algorithms/recursion/fibonacci
cpu: Intel(R) Core(TM) i5-3470S CPU @ 2.90GHz
BenchmarkFib40-4   	      	   2	 		649703540 ns/op	       0 B/op	       0 allocs/op
BenchmarkFibalt40-4   	29359101	            38.92 ns/op	       0 B/op	       0 allocs/op
*/

func BenchmarkFibAlt40(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FibAlt(40)
	}
}
