package single

import (
	"fmt"
	"strings"
)

type data interface {
	fmt.Stringer
	any
}

type Cell[T data] struct {
	Data    T
	Next    *Cell[T]
	Compare func(T, T) bool
	Equal   func(T, T) bool
}

// AddAfter adds a cell after me.
func (me *Cell[T]) AddAfter(cAfter *Cell[T]) {
	cAfter.Next = me.Next
	me.Next = cAfter
}

// DeleteAfter dels a cell after me.
func (me *Cell[T]) DeleteAfter() *Cell[T] {
	if me == nil {
		return nil
	}

	deleted := me.Next
	if deleted != nil {
		me.Next = deleted.Next
	}

	return deleted
}

func (me *Cell[T]) String() string {
	return me.Data.String()
}

type LinkedList[T data] struct {
	Sentinel *Cell[T]
}

func NewLinkedList[T data](comp, eq func(T, T) bool) *LinkedList[T] {
	return &LinkedList[T]{Sentinel: &Cell[T]{Compare: comp, Equal: eq}}
}

func (list *LinkedList[T]) AddRange(values []T) {
	lastCell := list.Sentinel
	for cell := lastCell.Next; cell != nil; cell = cell.Next {
		lastCell = cell
	}

	for _, c := range values {
		cCell := &Cell[T]{Data: c}
		lastCell.AddAfter(cCell)
		lastCell = cCell
	}
}

func (list *LinkedList[T]) ToString(separator string) string {
	out := make([]string, 0)
	for cell := list.Sentinel.Next; cell != nil; cell = cell.Next {
		out = append(out, cell.Data.String())
	}

	return strings.Join(out, separator)
}

func (list *LinkedList[T]) Len() int {
	count := 0
	for cell := list.Sentinel.Next; cell != nil; cell = cell.Next {
		count++
	}

	return count
}

func (list *LinkedList[T]) IsEmpty() bool {
	return list.Sentinel.Next == nil
}

func (list *LinkedList[T]) Contains(b T) bool {
	for cell := list.Sentinel.Next; cell != nil; cell = cell.Next {
		if list.Sentinel.Equal(cell.Data, b) {
			return true
		}
	}

	return false
}

func (list *LinkedList[T]) Push(c *Cell[T]) {
	list.Sentinel.AddAfter(c)
}

func (list *LinkedList[T]) Pop() *Cell[T] {
	return list.Sentinel.DeleteAfter()
}
