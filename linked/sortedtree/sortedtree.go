package sortedtree

import "fmt"

//

type data interface {
	fmt.Stringer
	any
}

type Node[T data] struct {
	data  T
	left  *Node[T]
	right *Node[T]
}

func (n *Node[T]) insertValue(newData T, compare func(T, T) bool) {
	if compare(n.data, newData) {
		if n.left != nil {
			n.left.insertValue(newData, compare)
			return
		}
		n.left = &Node[T]{data: newData}
		return
	}

	if n.right != nil {
		n.right.insertValue(newData, compare)
		return
	}
	n.right = &Node[T]{data: newData}
}

func (n *Node[T]) findValue(targetValue T, eq, compare func(a, b T) bool) *Node[T] {
	if eq(n.data, targetValue) {
		return n
	}

	if compare(n.data, targetValue) {
		if n.left != nil {
			return n.left.findValue(targetValue, eq, compare)
		}
		return nil
	}

	if n.right != nil {
		return n.right.findValue(targetValue, eq, compare)
	}
	return nil
}

func (n *Node[T]) inorder() string {
	result := ""

	if n.left != nil {
		result += n.left.inorder()
	}
	result += fmt.Sprintf("%s ", n.data)

	if n.right != nil {
		result += n.right.inorder()
	}

	return result
}

type SortedBiTree[T data] struct {
	sentinel *Node[T]
	compare  func(T, T) bool
	equal    func(T, T) bool
}

// NewSortedBiTree creates sorted binary tree.
func NewSortedBiTree[T data](compare, equal func(T, T) bool) *SortedBiTree[T] {
	root := Node[T]{}

	return &SortedBiTree[T]{
		sentinel: &root,
		compare:  compare,
		equal:    equal,
	}
}

func (sbt *SortedBiTree[T]) InsertValue(newData T) {
	sbt.sentinel.insertValue(newData, sbt.compare)
}

func (sbt *SortedBiTree[T]) FindValue(targetValue T) *Node[T] {
	return sbt.sentinel.findValue(targetValue, sbt.equal, sbt.compare)
}

func (sbt *SortedBiTree[T]) Inorder() string {
	return sbt.sentinel.inorder()
}
