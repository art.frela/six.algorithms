package loops

import (
	"fmt"
	"strings"
)

type data interface {
	fmt.Stringer
	any
}

type Cell[T data] struct {
	Data    T
	Next    *Cell[T]
	Compare func(T, T) bool
	Equal   func(T, T) bool
}

// AddAfter adds a cell after me.
func (me *Cell[T]) AddAfter(newC *Cell[T]) {
	newC.Next = me.Next
	me.Next = newC
}

func (me *Cell[T]) String() string {
	return me.Data.String()
}

type LinkedList[T data] struct {
	Sentinel *Cell[T]
}

func NewLinkedList[T data](comp, eq func(T, T) bool) *LinkedList[T] {
	return &LinkedList[T]{Sentinel: &Cell[T]{Compare: comp, Equal: eq}}
}

func (list *LinkedList[T]) AddRange(values []T) {
	lastCell := list.Sentinel
	for cell := lastCell.Next; cell != nil; cell = cell.Next {
		lastCell = cell
	}

	for _, c := range values {
		cCell := &Cell[T]{Data: c}
		lastCell.AddAfter(cCell)
		lastCell = cCell
	}
}

// ToString prints all list items.
func (list *LinkedList[T]) ToString(separator string) string {
	out := make([]string, 0)

	for cell := list.Sentinel.Next; cell != nil; cell = cell.Next {
		out = append(out, cell.Data.String())
	}

	return strings.Join(out, separator)
}

// ToStringMax prints all list items, but maximum printed items = max,
// for endless loop protection.
func (list *LinkedList[T]) ToStringMax(separator string, max int) string {
	out := make([]string, 0, max)
	i := 0
	for cell := list.Sentinel.Next; cell != nil; cell = cell.Next {
		out = append(out, cell.Data.String())

		if i >= max {
			break
		}
		i++
	}

	return strings.Join(out, separator)
}

// HasLoop implements [Floyd's tortoise and hare algorithm]
//
// [Floyd's tortoise and hare algorithm]: https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_tortoise_and_hare
func (list *LinkedList[T]) HasLoop() bool {
	if list == nil || list.Sentinel == nil {
		return false
	}

	slow := list.Sentinel.Next

	if slow == nil {
		return false
	}

	for fast := list.Sentinel.Next.Next; fast != nil; {
		if fast == slow {
			return true
		}
		slow = slow.Next

		if fast.Next == nil {
			return false
		}
		fast = fast.Next.Next
	}

	return false
}
