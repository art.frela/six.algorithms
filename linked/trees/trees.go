package trees

import (
	"fmt"
	"strings"

	"gitlab.com/art.frela/six.algorithms/linked/double"
)

type Node struct {
	data  string
	left  *Node
	right *Node
}

func (n *Node) String() string {
	return n.data
}

func (n *Node) DisplayIndented(indent string, depth int) string {
	result := fmt.Sprintf("%s%s\n", strings.Repeat(indent, depth), n.data)
	depth++
	if n.left != nil {
		result += n.left.DisplayIndented(indent, depth)
	}
	if n.right != nil {
		result += n.right.DisplayIndented(indent, depth)
	}

	return result
}

func (n *Node) Preorder() string {
	result := fmt.Sprintf("%s ", n.data)

	if n.left != nil {
		result += n.left.Preorder()
	}
	if n.right != nil {
		result += n.right.Preorder()
	}

	return result
}

func (n *Node) Inorder() string {
	result := ""

	if n.left != nil {
		result += n.left.Inorder()
	}
	result += fmt.Sprintf("%s ", n.data)

	if n.right != nil {
		result += n.right.Inorder()
	}

	return result
}

func (n *Node) Postorder() string {
	result := ""

	if n.left != nil {
		result += n.left.Postorder()
	}

	if n.right != nil {
		result += n.right.Postorder()
	}

	result += fmt.Sprintf("%s ", n.data)
	return result
}

func (n *Node) BreadthFirst() string {
	result := ""

	queue := double.NewDoublyLinkedList[*Node]()

	queue.Enqueue(n)

	for !queue.IsEmpty() {
		nn := queue.Dequeue()
		if nn == nil {
			continue
		}

		result += fmt.Sprintf("%s ", nn.data)

		if n.left != nil {
			queue.Enqueue(nn.left)
		}

		if n.right != nil {
			queue.Enqueue(nn.right)
		}
	}

	return result
}

func BuildTree() *Node {
	aNode := Node{data: "A"}
	bNode := Node{data: "B"}
	cNode := Node{data: "C"}
	//
	aNode.left = &bNode
	aNode.right = &cNode
	//
	dNode := Node{data: "D"}
	eNode := Node{data: "E"}
	fNode := Node{data: "F"}
	bNode.left = &dNode
	bNode.right = &eNode
	cNode.right = &fNode
	//
	gNode := Node{data: "G"}
	hNode := Node{data: "H"}
	eNode.left = &gNode
	fNode.left = &hNode
	//
	iNode := Node{data: "I"}
	jNode := Node{data: "J"}
	hNode.left = &iNode
	hNode.right = &jNode

	return &aNode
}
