package double

import (
	"fmt"
	"strings"
)

type data interface {
	fmt.Stringer
	any
}

type Cell[T data] struct {
	Data T
	Next *Cell[T]
	Prev *Cell[T]
}

// AddAfter adds a cell after me.
func (me *Cell[T]) AddAfter(newC *Cell[T]) {
	newC.Next = me.Next
	newC.Prev = me
	if me.Next != nil {
		me.Next.Prev = newC
	}
	me.Next = newC
}

// AddBefore adds a cell before me,
// Tip: Don’t write it from scratch,
// find a way to call the addAfter function.
func (me *Cell[T]) AddBefore(newC *Cell[T]) {
	me.Prev.AddAfter(newC)
}

// Delete dels a cell, returns deleted cell.
func (me *Cell[T]) Delete() *Cell[T] {
	if me == nil {
		return nil
	}

	if me.Prev != nil {
		me.Prev.Next = me.Next
	}

	if me.Next != nil {
		me.Next.Prev = me.Prev
	}

	return me
}

func (me *Cell[T]) String() string {
	return me.Data.String()
}

type DoublyLinkedList[T data] struct {
	TopSentinel    *Cell[T]
	BottomSentinel *Cell[T]
	Compare        func(T, T) bool
	Equal          func(T, T) bool
}

// NewDoublyLinkedList creates DoublyLinkedList.
func NewDoublyLinkedList[T data]() *DoublyLinkedList[T] {
	top := Cell[T]{}
	bottom := Cell[T]{}
	top.Next = &bottom
	bottom.Prev = &top

	return &DoublyLinkedList[T]{
		TopSentinel:    &top,
		BottomSentinel: &bottom,
	}
}

func (list *DoublyLinkedList[T]) AddRange(values []T) {
	first := list.TopSentinel
	for _, c := range values {
		cCell := &Cell[T]{Data: c}
		first.AddAfter(cCell)
		first = cCell
	}
}

func (list *DoublyLinkedList[T]) ToString(separator string) string {
	out := make([]string, 0)
	for cell := list.TopSentinel.Next; cell != list.BottomSentinel; cell = cell.Next {
		out = append(out, cell.Data.String())
	}

	return strings.Join(out, separator)
}

func (list *DoublyLinkedList[T]) IsEmpty() bool {
	return list.TopSentinel.Next == list.BottomSentinel
}

func (list *DoublyLinkedList[T]) Enqueue(value T) {
	newC := &Cell[T]{Data: value}
	list.TopSentinel.AddAfter(newC)
}

func (list *DoublyLinkedList[T]) Dequeue() T {
	deleted := list.BottomSentinel.Prev.Delete()
	return deleted.Data
}

func (list *DoublyLinkedList[T]) PushBottom(value T) {
	newC := &Cell[T]{Data: value}
	list.BottomSentinel.AddBefore(newC)
}

func (list *DoublyLinkedList[T]) PushTop(value T) {
	list.Enqueue(value)
}

func (list *DoublyLinkedList[T]) PopBottom() T {
	return list.Dequeue()
}

func (list *DoublyLinkedList[T]) PopTop() T {
	deleted := list.TopSentinel.Next.Delete()
	return deleted.Data
}
