package genslice

import (
	"fmt"
	"math/rand"

	"golang.org/x/exp/slices"
)

const defNumItems = 10

// MakeRandomSlice creates []int with length numItems, filled pseudo rand int.
func MakeRandomSlice(numItems, max int) []int {
	out := make([]int, numItems)

	for i := 0; i < numItems; i++ {
		out[i] = rand.Intn(max)
	}

	return out
}

// PrintSlice prints to stdout first numItems elements from slice,
// if numItems <=0 then use 10.
func PrintSlice(slice []int, numItems int) {
	if numItems <= 0 {
		numItems = defNumItems
	}

	if len(slice) <= numItems {
		fmt.Printf("%+v\n", slice)
		return
	}

	fmt.Printf("%+v\n", slice[:numItems])
}

// Verify that the slice is sorted.
func CheckSorted(slice []int) {
	if slices.IsSorted(slice) {
		fmt.Println("The slice is sorted")
		return
	}

	fmt.Println("The slice is NOT sorted!")
}
