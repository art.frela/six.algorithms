package genslice

import (
	"testing"
)

func TestMakeRandomSlice(t *testing.T) {
	tests := []struct {
		name     string
		numItems int
		max      int
	}{
		{
			"test.1 prints 5 el",
			5,
			100,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := MakeRandomSlice(tt.numItems, tt.max)
			if len(got) != tt.numItems {
				t.Errorf("MakeRandomSlice().Len = %d, want %d", len(got), tt.numItems)
			}

			CheckSorted(got)
			PrintSlice(got, 0)
		})
	}
}
