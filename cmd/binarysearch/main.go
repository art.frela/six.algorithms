package main

import (
	"fmt"
	"slices"

	"gitlab.com/art.frela/six.algorithms/sortandsearch/binary"
	"gitlab.com/art.frela/six.algorithms/utils/genslice"
)

func main() {
	// Get the number of items and maximum item value.
	var numItems, max, target int
	fmt.Printf("# Items: ")
	fmt.Scanln(&numItems)

	fmt.Printf("Max: ")
	fmt.Scanln(&max)

	// Make and display an unsorted slice.
	slice := genslice.MakeRandomSlice(numItems, max)
	// sort result slice
	slices.Sort(slice)

	genslice.PrintSlice(slice, 40)
	fmt.Println()

	for {
		fmt.Printf("Target: ")
		_, err := fmt.Scanln(&target)
		if err != nil {
			fmt.Println("Incorrect input, try again")
			continue
		}

		ix, num := binary.Search(slice, target)
		if ix < 0 {
			fmt.Printf("Target %d not found, %d tests\n", target, num)
			continue
		}

		fmt.Printf("values[%d] = %d, %d tests\n", ix, target, num)
	}
}
