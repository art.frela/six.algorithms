// Factorial
package main

import (
    "fmt"
    "math/big"
    )

func main() {
    var n int64
    for n = 0; n <= 21; n++ {
        fmt.Printf("%3d! = %20d\n", n, factorial(n))
        //fmt.Printf("%3d! = %20d (Iterative)\n", n, iterative_factorial(n))
        //fmt.Printf("%3d! = %20d (Big)\n", n, big_factorial(big.NewInt(n)))
    }
    fmt.Println()

    //fmt.Printf("%3d! = %d (Big)\n", 100, big_factorial(big.NewInt(100)))
}

func factorial(n int64) int64 {
    if n <= 1 { return 1  }

    return n * factorial(n - 1)
}

func iterative_factorial(n int64) int64 {
    result := int64(1)
    for i := int64(2); i <= int64(n); i++ {
        result *= i
    }

    return result
}


func big_factorial(n *big.Int) *big.Int {
    one := big.NewInt(1)
    if n.Cmp(one) <= 0 { return big.NewInt(1) }

    var n_minus_1 big.Int
    n_minus_1.Sub(n, one)

    var result big.Int
    result.Mul(n, big_factorial(&n_minus_1))
    return &result
}
