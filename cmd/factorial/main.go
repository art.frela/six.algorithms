package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/recursion/factorial"
)

func main() {
	var n int64
	for n = 0; n <= 21; n++ {
		fmt.Printf("%3d! = %20d\n", n, factorial.Factorial(n))
	}
	fmt.Println()
}
