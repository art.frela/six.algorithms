// Sorted binary tree.

package main

import (
    "fmt"
    "strings"
)

type Node struct {
    data        string
    left, right *Node
}

func main() {
    // Make a root node to act as sentinel.
    root := Node { "", nil, nil }

    // Add some values.
    root.insert_value("I")
    root.insert_value("G")
    root.insert_value("C")
    root.insert_value("E")
    root.insert_value("B")
    root.insert_value("K")
    root.insert_value("S")
    root.insert_value("Q")
    root.insert_value("M")

    // Add F.
    root.insert_value("F")

    // Display the values in sorted order.
    fmt.Printf("Sorted values: %s\n", root.right.inorder())

    // Let the user search for values.
    for {
        // Get the target value.
        target := ""
        fmt.Printf("String: ")
        fmt.Scanln(&target)
        if len(target) == 0 { break }

        // Find the value's node.
        node := root.find_value(target)
        if node == nil {
            fmt.Printf("%s not found\n", target)
        } else {
            fmt.Printf("Found value %s\n", target)
        }
    }
}

func (node *Node) insert_value(new_data string) {
    new_node := Node { new_data, nil, nil }
    for {
        if new_data <= node.data {
            // Add to left subtee.
            if node.left == nil {
                node.left = &new_node
                return
            }
            node = node.left
        } else {
            // Add to right subtree.
            if node.right == nil {
                node.right = &new_node
                return
            }
            node = node.right
        }
    }
}

func (node *Node) find_value(new_data string) *Node {
    for {
        if new_data == node.data {
            // We found it.
            return node
        } else if new_data <= node.data {
            // Search the left subtree.
            if node.left == nil { return nil }
            node = node.left
        } else {
            // Search the right subtree.
            if node.right == nil { return nil }
            node = node.right
        }
    }
}

func (node *Node) display_indented(indent string, depth int) string {
    result := strings.Repeat(indent, depth) + node.data + "\n"

    if node.left != nil {
        result += node.left.display_indented(indent, depth + 1)
    }
    if node.right != nil {
        result += node.right.display_indented(indent, depth + 1)
    }
    return result
}

func (node *Node) preorder() string {
    result := node.data

    if node.left != nil {
        result += " " + node.left.preorder()
    }

    if node.right != nil {
        result += " " + node.right.preorder()
    }

    return result
}

func (node *Node) inorder() string {
    result := ""

    if node.left != nil {
        result += node.left.inorder() + " "
    }

    result += node.data

    if node.right != nil {
        result += " " + node.right.inorder()
    }

    return result
}

func (node *Node) postorder() string {
    result := ""

    if node.left  != nil {
        result += node.left.postorder() + " "
    }

    if node.right != nil {
        result += node.right.postorder() + " "
    }

    result += node.data

    return result
}

func (root *Node) breadth_first() string {
    // Make a queue and add the root node.
    queue := make_doubly_linked_list()
    queue.enqueue(root)

    // Process the queue until it is empty.
    result := ""
    for !queue.is_empty() {
        // Get and process the next node.
        node := queue.dequeue()
        result += node.data

        // Add the node's children to the queue.
        if node.left  != nil { queue.enqueue(node.left) }
        if node.right != nil { queue.enqueue(node.right) }

        // If the queue is not empty, add a space.
        if !queue.is_empty() { result += " " }
    }

    return result
}


// *** DoublyLinkedList code ***
type Cell struct {
    data        *Node
    next, prev  *Cell
}

type DoublyLinkedList struct {  
    top_sentinel, bottom_sentinel   *Cell
}

// Make a new DoublyLinkedList and initialize its sentinels.
func make_doubly_linked_list() DoublyLinkedList {
    list := DoublyLinkedList { }

    top_cell := Cell { nil, nil, nil }
    bottom_cell := Cell { nil, nil, nil }

    list.top_sentinel = &top_cell
    list.top_sentinel.next = &bottom_cell
    list.top_sentinel.prev = nil

    list.bottom_sentinel = &bottom_cell
    list.bottom_sentinel.prev = list.top_sentinel
    list.bottom_sentinel.next = nil
    return list
}

// Add a cell after me.
func (me *Cell) add_after(after *Cell) {
    after.next = me.next
    after.prev = me

    me.next.prev = after
    me.next = after
}

// Add a cell before me.
func (me *Cell) add_before(before *Cell) {
    me.prev.add_after(before)
}

// Delete the cell and return it.
func (me *Cell) delete() *Cell {
    me.prev.next = me.next
    me.next.prev = me.prev
    return me
}

// Return true if the stack is empty, false otherwise.
func (stack *DoublyLinkedList) is_empty() bool {
    return stack.top_sentinel.next == stack.bottom_sentinel
}

// *** Stack functions ***

// Push an item onto the top of the list right after the sentinel.
func (stack *DoublyLinkedList) push(node *Node) {
    cell := Cell { data: node }
    stack.top_sentinel.add_after(&cell)
}

// Pop an item off of the list (from right after the sentinel).
func (stack *DoublyLinkedList) pop() *Node {
    if stack.is_empty() {
        panic("Cannot pop from an empty list")
    }

    return stack.top_sentinel.next.delete().data
}


// *** Queue functions ***

// Add an item to the top of the queue.
func (queue *DoublyLinkedList) enqueue(node *Node) {
    queue.push(node)
}

// Remove an item from the bottom of the queue.
func (queue *DoublyLinkedList) dequeue() *Node {
    if queue.is_empty() {
        panic("Cannot dequeue from an empty list")
    }
    return queue.bottom_sentinel.prev.delete().data
}

