package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/linked/sortedtree"
)

func main() {
	biTree := sortedtree.NewSortedBiTree[letter](compare, eq)

	biTree.InsertValue("I")
	biTree.InsertValue("G")
	biTree.InsertValue("C")
	biTree.InsertValue("E")
	biTree.InsertValue("B")
	biTree.InsertValue("K")
	biTree.InsertValue("S")
	biTree.InsertValue("Q")
	biTree.InsertValue("M")

	// Add F.
	biTree.InsertValue("F")

	// Display the values in sorted order.
	fmt.Printf("Sorted values: %s\n", biTree.Inorder())

	// Let the user search for values.
	for {
		// Get the target value.
		target := ""
		fmt.Printf("String: ")
		fmt.Scanln(&target)
		if len(target) == 0 {
			break
		}

		// Find the value's node.
		node := biTree.FindValue(letter(target))
		if node == nil {
			fmt.Printf("%s not found\n", target)
		} else {
			fmt.Printf("Found value %s\n", target)
		}
	}
}

type letter string

func (f letter) String() string {
	return string(f)
}

func compare(a, b letter) bool {
	return a > b
}

func eq(a, b letter) bool {
	return a == b
}
