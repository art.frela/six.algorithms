package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/linked/trees"
)

func main() {
	aNode := trees.BuildTree()

	fmt.Println(aNode.DisplayIndented("  ", 0))
	fmt.Println("Preorder:\t", aNode.Preorder())
	fmt.Println("Inorder:\t", aNode.Inorder())
	fmt.Println("Postorder:\t", aNode.Postorder())
	fmt.Println("Breadth first:\t", aNode.BreadthFirst())
}
