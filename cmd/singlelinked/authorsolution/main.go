// Linked list

package main

import "fmt"

type Cell struct {
	data string
	next *Cell
}

type LinkedList struct {
	sentinel *Cell
}

func main() {
	// small_list_test()

	// Make a list from an array of values.
	greek_letters := []string{
		"α", "β", "γ", "δ", "ε",
	}
	list := make_linked_list()
	list.add_range(greek_letters)
	fmt.Println(list.to_string(" "))
	fmt.Println()

	// Demonstrate a stack.
	stack := make_linked_list()
	stack.push("Apple")
	stack.push("Banana")
	stack.push("Coconut")
	stack.push("Date")
	for !stack.is_empty() {
		fmt.Printf("Popped: %-7s   Remaining %d: %s\n",
			stack.pop(),
			stack.length(),
			stack.to_string(" "))
	}
}

// Build a small linked list one cell at a time.
func small_list_test() {
	a_cell := Cell{"Apple", nil}
	b_cell := Cell{data: "Banana"}
	a_cell.next = &b_cell
	top := &a_cell

	for cell := top; cell != nil; cell = cell.next {
		fmt.Printf("%s ", cell.data)
	}
	fmt.Println()
}

// Add a cell after me.
func (me *Cell) add_after(after *Cell) {
	after.next = me.next
	me.next = after
}

// Delete the cell after me.
// Return the deleted cell.
func (me *Cell) delete_after() *Cell {
	// Make sure there *is* a following cell.
	if me.next == nil {
		panic("There is no cell after this one to delete")
	}

	// Return the following cell.
	after := me.next
	me.next = after.next
	return after
}

// Make a new LinkedList and initialize its sentinel.
func make_linked_list() LinkedList {
	list := LinkedList{}
	list.sentinel = &Cell{"SENTINEL", nil}
	return list
}

// Make a linked list containing the values.
func (list *LinkedList) add_range(values []string) {
	// Get a pointer to the last item in the list.
	last_cell := list.sentinel

	for last_cell.next != nil {
		last_cell = last_cell.next
	}

	// Loop to add the other cells.
	for _, value := range values {
		new_cell := Cell{value, nil}
		last_cell.add_after(&new_cell)
		last_cell = &new_cell
	}
}

// Return a string holding the cell values
// separated by the separator.
func (list *LinkedList) to_string(separator string) string {
	result := ""
	for cell := list.sentinel.next; cell != nil; cell = cell.next {
		result += cell.data
		if cell.next != nil {
			result += separator
		}
	}
	return result
}

// Return the number of cells in the list.
func (list *LinkedList) length() int {
	count := 0
	for cell := list.sentinel.next; cell != nil; cell = cell.next {
		count++
	}
	return count
}

// Return true if the stack is empty, false otherwise.
func (stack *LinkedList) is_empty() bool {
	return stack.sentinel.next == nil
}

// *** Stack functions ***

// Push an item onto the top of the list right aftr the sentinel.
func (stack *LinkedList) push(value string) {
	cell := Cell{data: value}
	stack.sentinel.add_after(&cell)
}

// Pop an item off of the list (from right after the sentinel).
func (stack *LinkedList) pop() string {
	return stack.sentinel.delete_after().data
}
