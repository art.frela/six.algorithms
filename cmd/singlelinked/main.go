package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/linked/single"
)

func main() {
	// smallListTest

	// Make a list from an array of values.
	greekLetters := []greekLetter{
		"α", "β", "γ", "δ", "ε",
	}
	list := single.NewLinkedList(CompareLet, EqualLet)

	list.AddRange(greekLetters)
	fmt.Println(list.ToString(" "))
	fmt.Println()

	// Demonstrate a stack.
	stack := single.NewLinkedList[fruit](Compare, Equal)

	stack.Push(&single.Cell[fruit]{Data: fruit("Apple")})
	stack.Push(&single.Cell[fruit]{Data: fruit("Banana")})
	stack.Push(&single.Cell[fruit]{Data: fruit("Coconut")})
	stack.Push(&single.Cell[fruit]{Data: fruit("Date")})

	for !stack.IsEmpty() {
		fmt.Printf("Popped: %-7s   Remaining %d: %s\n",
			stack.Pop(),
			stack.Len(),
			stack.ToString(" "))
	}
}

type fruit string

func (f fruit) String() string {
	return string(f)
}

func Compare(a, b fruit) bool {
	return a < b
}

func Equal(a, b fruit) bool {
	return a == b
}

type greekLetter string

func (gl greekLetter) String() string {
	return string(gl)
}

func CompareLet(a, b greekLetter) bool {
	return a < b
}

func EqualLet(a, b greekLetter) bool {
	return a == b
}
