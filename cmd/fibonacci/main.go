package main

import (
	"fmt"
	"strconv"
	"time"

	. "gitlab.com/art.frela/six.algorithms/recursion/fibonacci"
)

func main() {
	for {
		// Get n as a string.
		var nString string
		fmt.Printf("N: ")
		fmt.Scanln(&nString)

		// If the n string is blank, break out of the loop.
		if len(nString) == 0 {
			break
		}

		// Convert to int and calculate the Fibonacci number.
		n, _ := strconv.ParseInt(nString, 10, 64)
		start := time.Now()
		fmt.Printf("fibonacci(%d) = %d take: %s\n", n, Fib(n), time.Since(start))
		start = time.Now()
		fmt.Printf("fibonacciFreeRecursion(%d) = %d take: %s\n", n, FibAlt(n), time.Since(start))
	}
}
