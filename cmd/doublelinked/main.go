package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/linked/double"
)

func main() {
	// Test queue functions.
	fmt.Printf("*** Queue Functions ***\n")
	queue := double.NewDoublyLinkedList[stones]()
	queue.Enqueue("Agate")
	queue.Enqueue("Beryl")
	fmt.Printf("%s ", queue.Dequeue())
	queue.Enqueue("Citrine")
	fmt.Printf("%s ", queue.Dequeue())
	fmt.Printf("%s ", queue.Dequeue())
	queue.Enqueue("Diamond")
	queue.Enqueue("Emerald")
	for !queue.IsEmpty() {
		fmt.Printf("%s ", queue.Dequeue())
	}
	fmt.Printf("\n\n")

	// Test deque functions. Names starting
	// with F have a fast pass.
	fmt.Printf("*** Deque Functions ***\n")
	deque := double.NewDoublyLinkedList[names]()
	deque.PushTop("Ann")
	deque.PushTop("Ben")
	fmt.Printf("%s ", deque.PopBottom())
	deque.PushBottom("F-Cat")
	fmt.Printf("%s ", deque.PopBottom())
	fmt.Printf("%s ", deque.PopBottom())
	deque.PushBottom("F-Dan")
	deque.PushTop("Eva")
	for !deque.IsEmpty() {
		fmt.Printf("%s ", deque.PopBottom())
	}
	fmt.Printf("\n")
}

type stones string

func (f stones) String() string {
	return string(f)
}

type names string

func (f names) String() string {
	return string(f)
}
