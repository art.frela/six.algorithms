// Doubly linked lists

package main

import "fmt"

func main() {
    /*
    // Doubly-linked list tests.
    // Make a list from an array of values.
    list := make_doubly_linked_list()
    animals := []string {
        "Ant",
        "Bat",
        "Cat",
        "Dog",
        "Elk",
        "Fox",
    }
    list.add_range(animals)
    fmt.Println(list.to_string(" "))
    fmt.Println()
    */

    // Test queue functions.
    fmt.Printf("*** Queue Functions ***\n")
    queue := make_doubly_linked_list()
    queue.enqueue("Agate")
    queue.enqueue("Beryl")
    fmt.Printf("%s ", queue.dequeue())
    queue.enqueue("Citrine")
    fmt.Printf("%s ", queue.dequeue())
    fmt.Printf("%s ", queue.dequeue())
    queue.enqueue("Diamond")
    queue.enqueue("Emerald")
    for !queue.is_empty() {
        fmt.Printf("%s ", queue.dequeue())
    }
    fmt.Printf("\n\n")

    // Test deque functions. Names starting
    // with F have a fast pass.
    fmt.Printf("*** Deque Functions ***\n")
    deque := make_doubly_linked_list()
    deque.push_top("Ann")
    deque.push_top("Ben")
    fmt.Printf("%s ", deque.pop_bottom())
    deque.push_bottom("F-Cat")
    fmt.Printf("%s ", deque.pop_bottom())
    fmt.Printf("%s ", deque.pop_bottom())
    deque.push_bottom("F-Dan")
    deque.push_top("Eva")
    for !deque.is_empty() {
        fmt.Printf("%s ", deque.pop_bottom())
    }
    fmt.Printf("\n")
}

type Cell struct {
    data        string
    next, prev  *Cell
}

type DoublyLinkedList struct {  
    top_sentinel, bottom_sentinel   *Cell
}

// Make a new DoublyLinkedList and initialize its sentinels.
func make_doubly_linked_list() DoublyLinkedList {
    list := DoublyLinkedList { }

    top_cell := Cell { "TOP", nil, nil }
    bottom_cell := Cell { "BOTTOM", nil, nil }

    list.top_sentinel = &top_cell
    list.top_sentinel.next = &bottom_cell
    list.top_sentinel.prev = nil

    list.bottom_sentinel = &bottom_cell
    list.bottom_sentinel.prev = list.top_sentinel
    list.bottom_sentinel.next = nil
    return list
}

// Add a cell after me.
func (me *Cell) add_after(after *Cell) {
    after.next = me.next
    after.prev = me

    me.next.prev = after
    me.next = after
}

// Add a cell before me.
func (me *Cell) add_before(before *Cell) {
    me.prev.add_after(before)
}

// Delete the cell and return it.
func (me *Cell) delete() *Cell {
    me.prev.next = me.next
    me.next.prev = me.prev
    return me
}

// Make a linked list containing the values.
func (list *DoublyLinkedList)add_range(values []string) {
    // Loop to add cells before the bottom sentinel.
    for _, value := range values {
        new_cell := Cell { value, nil, nil }
        list.bottom_sentinel.add_before(&new_cell)
    }
}

// Return a string holding the cell values
// separated by the separator.
func (list *DoublyLinkedList) to_string(separator string) string {
    result := ""
    for cell := list.top_sentinel.next; cell != list.bottom_sentinel; cell = cell.next {
        result += cell.data
        if cell.next != list.bottom_sentinel { result += separator }
    }
    return result
}

// Return a string holding the cell values
// separated by the separator.
// Show at most max cell values.
func (list *DoublyLinkedList) to_string_max(separator string, max int) string {
    count := 0
    result := ""
    for cell := list.top_sentinel.next; cell != list.bottom_sentinel; cell = cell.next {
        result += cell.data

        count += 1
        if count >= max { break }

        if cell.next != list.bottom_sentinel { result += separator }
    }
    return result
}

// Return the number of cells in the list.
func (list *DoublyLinkedList) length() int {
    count := 0
    for cell := list.top_sentinel.next; cell != list.bottom_sentinel; cell = cell.next {
        count++
    }
    return count
}

// Return true if the stack is empty, false otherwise.
func (stack *DoublyLinkedList) is_empty() bool {
    return stack.top_sentinel.next == stack.bottom_sentinel
}

// *** Stack functions ***

// Push an item onto the top of the list right after the sentinel.
func (stack *DoublyLinkedList) push(value string) {
    cell := Cell { data: value }
    stack.top_sentinel.add_after(&cell)
}

// Pop an item off of the list (from right after the sentinel).
func (stack *DoublyLinkedList) pop() string {
    if stack.is_empty() {
        panic("Cannot pop from an empty list")
    }

    return stack.top_sentinel.next.delete().data
}

// Return true if the list has a loop.
func (list *DoublyLinkedList) has_loop() bool {
    // Empty lists have no loop.
    if list.is_empty() { return false }

    fast := list.top_sentinel.next
    slow := fast

    for {
        fast = fast.next
        if fast == list.bottom_sentinel { return false }
        if fast == slow { return true }

        fast = fast.next
        if fast == list.bottom_sentinel { return false }
        if fast == slow { return true }

        slow = slow.next
        if fast == slow { return true }
    }
}


// *** Queue functions ***

// Add an item to the top of the queue.
func (queue *DoublyLinkedList) enqueue(value string) {
    queue.push(value)
}

// Remove an item from the bottom of the queue.
func (queue *DoublyLinkedList) dequeue() string {
    if queue.is_empty() {
        panic("Cannot dequeue from an empty list")
    }
    return queue.bottom_sentinel.prev.delete().data
}


// *** Deque functions ***

// Add an item at the top of the deque.
func (deque *DoublyLinkedList) push_top(value string) {
    deque.push(value)
}

// Add an item at the bottom of the deque.
func (deque *DoublyLinkedList) push_bottom(value string) {
    cell := Cell { data: value }
    deque.bottom_sentinel.add_before(&cell)
}

// Remove an item from the top of the deque.
func (deque *DoublyLinkedList) pop_top() string {
    if deque.is_empty() {
        panic("Cannot pop_top from an empty list")
    }
    return deque.pop()
}

// Add an item at the top of the deque.
func (deque *DoublyLinkedList) pop_bottom() string {
    if deque.is_empty() {
        panic("Cannot pop_botom from an empty list")
    }
    return deque.dequeue()
}
