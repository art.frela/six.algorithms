package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/sortandsearch/quick"
	"gitlab.com/art.frela/six.algorithms/utils/genslice"
)

func main() {
	// Get the number of items and maximum item value.
	var numItems, max int
	fmt.Printf("# Items: ")
	fmt.Scanln(&numItems)
	fmt.Printf("Max: ")
	fmt.Scanln(&max)

	// Make and display an unsorted slice.
	slice := genslice.MakeRandomSlice(numItems, max)
	genslice.PrintSlice(slice, 40)
	fmt.Println()

	// Sort and display the result.
	quick.Sort(slice)
	genslice.PrintSlice(slice, 40)

	// Verify that it's sorted.
	genslice.CheckSorted(slice)
}
