package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/sortandsearch/count"
)

func main() {
	// Get the number of items and maximum item value.
	var numItems, max int
	fmt.Printf("# Items: ")
	fmt.Scanln(&numItems)
	fmt.Printf("Max: ")
	fmt.Scanln(&max)

	// Make and display an unsorted slice.
	slice := count.MakeRandomSlice(numItems, max)
	count.PrintSlice(slice, 40)
	fmt.Println()

	// Sort and display the result.
	count.CountingSort(slice, max)
	count.PrintSlice(slice, 40)

	// Verify that it's sorted.
	count.CheckSorted(slice)
}
