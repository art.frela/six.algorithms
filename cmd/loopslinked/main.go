package main

import (
	"fmt"

	"gitlab.com/art.frela/six.algorithms/linked/loops"
)

func main() {
	// Make a list from an array of values.
	values := []fruit{"0", "1", "2", "3", "4", "5"}

	list := loops.NewLinkedList[fruit](Equal, Compare)
	list.AddRange(values)

	fmt.Println(list.ToString(" "))
	if list.HasLoop() {
		fmt.Println("Has loop")
	} else {
		fmt.Println("No loop")
	}
	fmt.Println()

	// Make cell 5 point to cell 2.
	list.Sentinel.Next.Next.Next.Next.Next.Next = list.Sentinel.Next.Next

	fmt.Println(list.ToStringMax(" ", 10))
	if list.HasLoop() {
		fmt.Println("Has loop")
	} else {
		fmt.Println("No loop")
	}
	fmt.Println()

	// Make cell 4 point to cell 2.
	list.Sentinel.Next.Next.Next.Next.Next = list.Sentinel.Next.Next

	fmt.Println(list.ToStringMax(" ", 10))
	if list.HasLoop() {
		fmt.Println("Has loop")
	} else {
		fmt.Println("No loop")
	}
}

type fruit string

func (f fruit) String() string {
	return string(f)
}

func Compare(a, b fruit) bool {
	return a < b
}

func Equal(a, b fruit) bool {
	return a == b
}
