// Linked list loop

package main

import "fmt"

type Cell struct {
    data        string
    next        *Cell
}

type LinkedList struct {
    sentinel    *Cell
}

func main() {
    // Make a list from an array of values.
    values := []string {
        "0", "1", "2", "3", "4", "5",
    }
    list := make_linked_list()
    list.add_range(values)

    fmt.Println(list.to_string(" "))
    if list.has_loop() {
        fmt.Println("Has loop")
    } else {
        fmt.Println("No loop")
    }
    fmt.Println()

    // Make cell 5 point to cell 2.
    list.sentinel.next.next.next.next.next.next = list.sentinel.next.next

    fmt.Println(list.to_string_max(" ", 10))
    if list.has_loop() {
        fmt.Println("Has loop")
    } else {
        fmt.Println("No loop")
    }
    fmt.Println()

    // Make cell 4 point to cell 2.
    list.sentinel.next.next.next.next.next = list.sentinel.next.next

    fmt.Println(list.to_string_max(" ", 10))
    if list.has_loop() {
        fmt.Println("Has loop")
    } else {
        fmt.Println("No loop")
    }
}

// Add a cell after me.
func (me *Cell) add_after(after *Cell) {
    after.next = me.next
    me.next = after
}

// Delete the cell after me.
// Return the deleted cell.
func (me *Cell) delete_after() *Cell {
    // Make sure there *is* a following cell.
    if me.next == nil { panic("There is no cell after this one to delete") }

    // Return the following cell.
    after := me.next
    me.next = after.next
    return after
}

// Make a new LinkedList and initialize its sentinel.
func make_linked_list() LinkedList {
    list := LinkedList { }
    list.sentinel = &Cell { "SENTINEL", nil }
    return list
}

// Make a linked list containing the values.
func (list *LinkedList) add_range(values []string) {
    // Get a pointer to the last item in the list.
    last_cell := list.sentinel

    for last_cell.next != nil {
        last_cell = last_cell.next
    }

    // Loop to add the other cells.
    for _, value := range values {
        new_cell := Cell { value, nil }
        last_cell.add_after(&new_cell)
        last_cell = &new_cell
    }
}

// Return a string holding the cell values
// separated by the separator.
func (list *LinkedList) to_string(separator string) string {
    result := ""
    for cell := list.sentinel.next; cell != nil; cell = cell.next {
        result += cell.data
        if cell.next != nil { result += separator }
    }
    return result
}

// Return a string holding the cell values
// separated by the separator.
// Show at most max cell values.
func (list *LinkedList) to_string_max(separator string, max int) string {
    count := 0
    result := ""
    for cell := list.sentinel.next; cell != nil; cell = cell.next {
        result += cell.data
        count++
        if count >= max { break }
        if cell.next != nil { result += separator }
    }
    return result
}

// Return the number of cells in the list.
func (list *LinkedList) length() int {
    count := 0
    for cell := list.sentinel.next; cell != nil; cell = cell.next {
        count++
    }
    return count
}

// Return true if the stack is empty, false otherwise.
func (stack *LinkedList) is_empty() bool {
    return stack.sentinel.next == nil
}

// *** Stack functions ***

// Push an item onto the top of the list right after the sentinel.
func (stack *LinkedList) push(value string) {
    cell := Cell { data: value }
    stack.sentinel.add_after(&cell)
}

// Pop an item off of the list (from right after the sentinel).
func (stack *LinkedList) pop() string {
    return stack.sentinel.delete_after().data
}

// Return true if the list has a loop.
func (list *LinkedList) has_loop() bool {
    // Empty lists have no loop.
    if list.is_empty() { return false }

    fast := list.sentinel.next
    slow := fast

    for {
        // If fast drops off the end, there's no loop.
        // If fast catches slow, there's is a loop.
        fast = fast.next
        if fast == nil { return false }
        if fast == slow { return true }

        fast = fast.next
        if fast == nil { return false }
        if fast == slow { return true }

        slow = slow.next
        if fast == slow { return true }
    }
}
